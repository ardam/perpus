<?php

use Illuminate\Database\Seeder;

class DendaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dendas')->insert([
        	'name'=>'500',
        ]);
    }
}
