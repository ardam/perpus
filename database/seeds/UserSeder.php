<?php

use Illuminate\Database\Seeder;

class UserSeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Aang',
            'email' => 'aang.ardam@gmail.com',
            'password' => bcrypt('111'),
        ]);
    }
}
