<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no')->uniqid();
            $table->string('nis')->uniqid();
            $table->string('name');
            $table->string('birthday');
            $table->string('placeofbirth');
            $table->enum('gender',['L','P']);
            $table->text('addres');
            $table->string('image')->nullable();
            $table->date('masaberlaku')->nullable();
            $table->string('ket')->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
