<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sistem Perpustakaan</title>
	<link href="{{asset('asst/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('asst/css/font-awesome.min.css')}}" rel="stylesheet">
	<link href="{{asset('asst/css/datepicker3.css')}}" rel="stylesheet">
	<link href="{{asset('asst/css/styles.css')}}" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
</head>
<body>
	<!-- NAVBAR -->
	@include('layouts.navbar')
	<!-- END NAVBAR -->
	<!-- SIDEBAR-->
	@include('layouts.sider')
	<!-- END SIDEBAR -->

	<!-- CONTENT -->
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		@yield('content')
	</div><!--/.main-->
	<!-- END CONTENT -->
	
<script src="{{asset('asst/js/jquery-1.11.1.min.js')}}"></script>
	<script src="{{asset('asst/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('asst/js/chart.min.js')}}"></script>
	<script src="{{asset('asst/js/chart-data.js')}}"></script>
	<script src="{{asset('asst/js/easypiechart.js')}}"></script>
	<script src="{{asset('asst/js/easypiechart-data.js')}}"></script>
	<script src="{{asset('asst/js/bootstrap-datepicker.js')}}"></script>
	<script src="{{asset('asst/js/custom.js')}}"></script>
	
</body>
</html>
