@extends('layouts.index')
@section('content')
<div class="row">
	<ol class="breadcrumb">
		<li><a href="{{url('/')}}">
			<em class="fa fa-home"></em>
		</a></li>
		<li class="active">Members</li>
	</ol>
</div><!--/.row-->

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Members</h1>
	</div>
</div><!--/.row-->

<div class="modal fade bs-example-modal-lg" id="addmember" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Form Tambah Anggota</h4>
			</div>
			<div class="modal-body">
				<form action="{{ url('member') }}" method="post" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label> NIS </label>
								<input type="text" name="nis" id="nis" class="form-control" required="" placeholder="Masukan NIS Siswa">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label> Nama </label>
								<input type="text" name="name" id="name" class="form-control" required="" placeholder="Masukan Nama">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label> Tempat Lahir </label>
								<input type="text" name="placeofbirth" id="placeofbirth" class="form-control" required="" placeholder="Masukan Tempat Lahir">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label> Tanggal Lahir </label>
								<input type="text" name="birthday" id="birthday" class="form-control" placeholder="Masukan Tanggal Lahir">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label> Jenis Kelamin </label>
								<select name="gender" id="gender" class="form-control" required="">
									<option value="" selected="" disabled="">Pilih Jenis Kelamin</option>
									<option value="L">Laki - Laki </option>
									<option value="P">Perempuan</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label> Alamat </label>
								<input type="text" name="addres" id="addres" class="form-control" placeholder="Masukan Alamat">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label> Foto </label>
								<input type="file" name="image" id="image" class="form-control" placeholder="Masukan Alamat">
							</div>
						</div>
					</div>

                    <hr>
					<button type="submit" class="btn btn-info btn-fill pull-right">Simpan</button>
					<div class="clearfix"></div>
				</form>
			</div>
		</div>
	</div>
</div>



<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Members List </div>
			<div class="panel-body">
				<div class="col-md-12">
					<table width="100%">
						<tr>
							<!-- <td> Books List </td> -->
							<td align="right">
								<!-- <a href="#!" data-togle="modal" data-target="#addbook"> Tambah Buku </a> -->
								<button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addmember">Tambah Anggota</button>
							</td>
						</tr>
					</table>
					@if(session()->has('success'))
	                    <div class="alert alert-success">
	                        {{ session()->get('success') }}
	                    </div>
	                @endif

	                @if(session()->has('error'))
	                    <div class="alert alert-warning">
	                        {{ session()->get('error') }}
	                    </div>
	                @endif
					 <table class="table table-hover">
						<thead>
							<tr>
								<td> No </td>
								<td> No Anggota </td>
								<td> NIS </td>
								<td> Nama </td>
								<td> Foto </td>
								<td> Masa Berlaku </td>
								<td> Keterangan </td>
								<td> Action </td>
							</tr>
						</thead>
						<tbody>
							@foreach($data as $key=>$value)
								<tr>
									<td>{{ $key+1 }}</td>
									<td>{{ $value->no }}</td>
									<td>{{ $value->nis }}</td>
									<td>{{ $value->name }}</td>
									
									<td>
										@if($value->image=='')
											@if($value->gender=='L')
												<!-- <img src="image/notfound/L.jpg" width="150"> -->
												<img src="{{ asset('image/anggota/notfound/L.jpg') }}" width="50">
											@else
												<img src="{{ asset('image/anggota/notfound/L.jpg') }}" width="50">
											@endif
										@else
											<img src="{{$value->image}}" width="150">
										@endif
									</td>
									<td>{{ $value->masaberlaku }}</td>
									<form action="{{ url('member') }}" method="post">
										{{ csrf_field() }}
										<input type="hidden" name="_method" value="DELETE">
										<?php $tgl = date('Y-m-d'); ?>
										@if( $value->masaberlaku > $tgl )
											<td> Active </td>
										@else
											<td>
												Expired (<a href="{{ url('memberperpanjang/'.$value->id ) }}"> Perpanjang </a>)
												
											</td>
										@endif
										<td>
											<button class="btn btn-success btn-xs" onclick="formeditnya('{{ $value->id }}')" type="button" data-toggle="modal" data-target="#formedit"> Ubah </button> ||
	                                		<button type="submit" class="btn btn-warning btn-xs" onclick="return confirm('Anda yakin akan menghapusnya?')" > Hapus </button>
	                                	</td>
									</form>
									
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div><!-- /.panel-->
	</div><!-- /.col-->

	<!-- FOOTER -->
	@include('layouts.sider')
	<!-- END FOOTER -->
</div><!-- /.row -->
<div class="modal fade bs-example-modal-lg" id="formedit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Form Ubah Anggota</h4>
			</div>
			<div class="modal-body">
				<div id="formnya"></div>
			</div>
		</div>
	</div>
</div>
<script>
function formeditnya(id,vId) {
	//alert(id);
    $.ajax({
          url: "../member/" +id+ "/edit",
          success: function(data){
              $("#formnya").html(data);
          }
    });
}
</script>
@endsection()