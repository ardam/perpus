@extends('layouts.index')
@section('content')
<div class="row">
	<ol class="breadcrumb">
		<li><a href="{{url('/')}}">
			<em class="fa fa-home"></em>
		</a></li>
		<li class="active">Denda</li>
	</ol>
</div><!--/.row-->

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Denda</h1>
	</div>
</div><!--/.row-->


<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Denda </div>
			<div class="panel-body">
				<div class="col-md-12">
					@if(session()->has('success'))
	                    <div class="alert alert-success">
	                        {{ session()->get('success') }}
	                    </div>
	                @endif

	                @if(session()->has('error'))
	                    <div class="alert alert-warning">
	                        {{ session()->get('error') }}
	                    </div>
	                @endif
					 <table class="table table-hover">
						<thead>
							<tr>
								<td> No </td>
								<td> Denda </td>
								<td> Action </td>
							</tr>
						</thead>
						<tbody>
							@foreach($data as $key=>$value)
								<tr>
									<td>{{ $key+1 }}</td>
									<td>{{ $value->name }}</td>
									<td>
										<form action="{{ url('book') }}" method="post">
											{{ csrf_field() }}
											<!-- <input type="hidden" name="_method" value="DELETE"> -->
											
											<button class="btn btn-success btn-xs" onclick="formeditnya('{{ $value->id }}')" type="button" data-toggle="modal" data-target="#formedit"> Ubah </button>
										</form>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div><!-- /.panel-->
	</div><!-- /.col-->

	<!-- FOOTER -->
	@include('layouts.sider')
	<!-- END FOOTER -->
</div><!-- /.row -->
<div class="modal fade bs-example-modal-lg" id="formedit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Form Ubah Denda</h4>
			</div>
			<div class="modal-body">
				<div id="formnya"></div>
			</div>
		</div>
	</div>
</div>
<script>
function formeditnya(id,vId) {
	//alert(id);
    $.ajax({
          url: "../denda/" +id+ "/edit",
          success: function(data){
              $("#formnya").html(data);
          }
    });
}
</script>
@endsection()