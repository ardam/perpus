@extends('layouts.index')
@section('content')
<div class="row">
	<ol class="breadcrumb">
		<li><a href="{{url('/')}}">
			<em class="fa fa-home"></em>
		</a></li>
		<li class="active">Books</li>
	</ol>
</div><!--/.row-->

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Books</h1>
	</div>
</div><!--/.row-->

<div class="modal fade bs-example-modal-lg" id="addbook" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Form Tambah Buku</h4>
			</div>
			<div class="modal-body">
				<form action="{{ url('books') }}" method="post" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label> Judul </label>
								<input type="text" name="name" id="name" class="form-control" required="" placeholder="Masukan judul Buku">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label> Pengarang </label>
								<input type="text" name="writer" id="writer" class="form-control" required="" placeholder="Masukan Pengarang Buku">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label> Penerbit </label>
								<input type="text" name="publisher" id="publisher" class="form-control" required="" placeholder="Masukan Penerbit Buku">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label> Posisi/Letak </label>
								<input type="text" name="position" id="position" class="form-control" placeholder="Masukan Posisi Buku">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label> Jumlah Buku </label>
								<input type="number" name="qty" id="qty" class="form-control" placeholder="Masukan Jumlah Buku">
							</div>
						</div>
					</div>

                    <hr>
					<button type="submit" class="btn btn-info btn-fill pull-right">Simpan</button>
					<div class="clearfix"></div>
				</form>
			</div>
		</div>
	</div>
</div>



<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Books List </div>
			<div class="panel-body">
				<div class="col-md-12">
					<table width="100%">
						<tr>
							<!-- <td> Books List </td> -->
							<td align="right">
								<!-- <a href="#!" data-togle="modal" data-target="#addbook"> Tambah Buku </a> -->
								<button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addbook">Tambah Buku</button>
							</td>
						</tr>
					</table>
					@if(session()->has('success'))
	                    <div class="alert alert-success">
	                        {{ session()->get('success') }}
	                    </div>
	                @endif

	                @if(session()->has('error'))
	                    <div class="alert alert-warning">
	                        {{ session()->get('error') }}
	                    </div>
	                @endif
					 <table class="table table-hover">
						<thead>
							<tr>
								<td> No </td>
								<td> Judul </td>
								<td> Pengarang </td>
								<td> Penerbit </td>
								<td> Posisi </td>
								<td> Status </td>
								<td> Action </td>
							</tr>
						</thead>
						<tbody>
							@foreach($data as $key=>$value)
								<tr>
									<td>{{ $key+1 }}</td>
									<td>{{ $value->name }}</td>
									<td>{{ $value->writer }}</td>
									<td>{{ $value->publisher }}</td>
									<td>{{ $value->position }}</td>
									<td>{{ $value->status }}</td>
									<td>
										<form action="{{ url('book') }}" method="post">
											{{ csrf_field() }}
											<input type="hidden" name="_method" value="DELETE">
											
											<button class="btn btn-success btn-xs" onclick="formeditnya('{{ $value->id }}')" type="button" data-toggle="modal" data-target="#formedit"> Ubah </button> ||

                                    		<button type="submit" class="btn btn-warning btn-xs" onclick="return confirm('Anda yakin akan menghapusnya?')" > Hapus </button>
										</form>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div><!-- /.panel-->
	</div><!-- /.col-->

	<!-- FOOTER -->
	@include('layouts.sider')
	<!-- END FOOTER -->
</div><!-- /.row -->
<div class="modal fade bs-example-modal-lg" id="formedit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Form Ubah Buku</h4>
			</div>
			<div class="modal-body">
				<div id="formnya"></div>
			</div>
		</div>
	</div>
</div>
<script>
function formeditnya(id,vId) {
	//alert(id);
    $.ajax({
          url: "../books/" +id+ "/edit",
          success: function(data){
              $("#formnya").html(data);
          }
    });
}
</script>
@endsection()