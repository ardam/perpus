@extends('layouts.index')


@section('content')
<div class="row">
	<ol class="breadcrumb">
		<li><a href="{{url('/')}}">
			<em class="fa fa-home"></em>
		</a></li>
		<li class="active">Beranda</li>
	</ol>
</div><!--/.row-->

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Beranda</h1>
	</div>
</div><!--/.row-->


<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Selamat Datang</div>
			<div class="panel-body">
				<div class="col-md-12">
					<p></p>
				</div>
			</div>
		</div><!-- /.panel-->
	</div><!-- /.col-->

	<!-- FOOTER -->
	@include('layouts.sider')
	<!-- END FOOTER -->
</div><!-- /.row -->
@endsection()