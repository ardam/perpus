<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\denda;

class DendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = denda::all();
        return view('admin.denda',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = denda::find($id);
        echo '
            <form action="'.url('denda/'.$data->id) .'" method = "post">
               '.csrf_field().'
               <input type="hidden" name="id" class="form-control" value="'.$data->id.'">
               <input type="hidden" name="_method" value="put">
               <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label> Denda </label>
                                <input type="text" name="name" id="name" class="form-control" required="" placeholder="Masukan Denda" value="'.$data->name.'">
                        </div>
                    </div>
                </div>
                <hr>
                <button type="submit" class="btn btn-info btn-fill pull-right">Simpan</button>
                <div class="clearfix"></div>
            </form>

        ';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->id;
        $name = $request->name;
        $denda = denda::where('id',$id)->update(array(
            'name'  => $name
        ));
        //return $denda;
        if ($denda) {
           return redirect('denda')->with('success','Data Denda Berhasil di Ubah');
        }else{
            return redirect('denda')->with('success','Data Denda Berhasil di Ubah');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
