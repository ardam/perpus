<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\book;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = book::all();
        return view('admin.buku',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $buku = book::create($data);
        if ($buku) {
            return redirect('books')->with('success','Data Buku Berhasil di Simpan');
        }else{
            return redirect('books')->with('error','Data Buku Gagal di Simpan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = book::find($id);
        echo '
                <form action="'.url('books/'.$data->id).'" method="post" enctype="multipart/form-data">
                    '.csrf_field().'
                    <input type="hidden" name="id" class="form-control" value="'.$data->id.'">
                    <input type="hidden" name="_method" value="put">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Judul </label>
                                <input type="text" name="name" id="name" class="form-control" required="" placeholder="Masukan judul Buku" value="'.$data->name.'">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Pengarang </label>
                                <input type="text" name="writer" id="writer" class="form-control" required="" placeholder="Masukan Pengarang Buku" value="'.$data->writer.'">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Penerbit </label>
                                <input type="text" name="publisher" id="publisher" class="form-control" required="" placeholder="Masukan Penerbit Buku" value="'.$data->publisher.'">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Posisi/Letak </label>
                                <input type="text" name="position" id="position" class="form-control" placeholder="Masukan Posisi Buku" value="'.$data->position.'">
                            </div>
                        </div>
                    </div>

                    <hr>
                    <button type="submit" class="btn btn-info btn-fill pull-right">Simpan</button>
                    <div class="clearfix"></div>
                </form>

        ';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->id;
        $name = $request->name;
        $writer = $request->writer;
        $publisher = $request->publisher;
        $position = $request->position;
        $buku = buku::where('id',$id)->update(array(
            'name'      => $name,
            'writer'    => $writer,
            'publisher' => $publisher,
            'position'  => $position
        ));
        if ($data) {
            return redirect('books')->with('success','Data Buku Berhasil di Ubah');
        }else{
            return redirect('books')->with('error','Data Buku Gagal di Ubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = book::find($id)->delete();
        if ($data) {
            return redirect('books')->with('success','Data Buku Berhasil di Hapus');
        }else{
            return redirect('books')->with('error','Data Buku Gagal di Hapus');
        }
    }
}
