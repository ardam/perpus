<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\member;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = member::all();
        return view('admin.anggota',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $thn1 = date('Y-m-d');
        $thn2 = date('Y-m-d', strtotime('+1 year',strtotime($thn1)));
        //return $thn2;

        $data = $request->all();
        $nis = $request->nis;
        if ($request->has('image')) {
            $member = explode(".",$request->file('image')->getClientOriginalName());
            $member = $nis."-".$data['name']."_".date("YmdHis").strtolower('.'.$member[1]);
            $tempat ="image/anggota";
            $request->file('image')->move($tempat,$member);
            $data['image']=$tempat."/".$member;
        }
        
        $data['masaberlaku']=$thn2;
        $data['no']=(substr($thn1, -8,2)).(substr($thn2,-8,2)).$nis;
        $simpan = member::create($data);
        if ($simpan) {
            return redirect('member')->with('success','Data Anggota Berhasil di Simpan');
        }else{
            return redirect('member')->with('error','Data Anggota Gagal di Simpan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = member::find($id);
        $jk ='';
        if ($data['gender']=='L') {
            $jk.='<option value="L" selected> Laki-Laki </option>';
        }else{
             $jk.='<option value="P" selected> Perempuan </option>';
        }
        echo '
                <form action="'.url('member/'.$data->id).'" method="post" enctype="multipart/form-data">
                    '.csrf_field().'
                    <input type="hidden" name="id" class="form-control" value="'.$data->id.'">
                    <input type="hidden" name="_method" value="put">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label> NIS </label>
                                <input type="text" name="nis" id="nis" class="form-control" required="" placeholder="Masukan NIS Siswa" value="'.$data->nis.'">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Nama </label>
                                <input type="text" name="name" id="name" class="form-control" required="" placeholder="Masukan Nama" value="'.$data->name.'">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Tempat Lahir </label>
                                <input type="text" name="placeofbirth" id="placeofbirth" class="form-control" required="" placeholder="Masukan Tempat Lahir" value="'.$data->placeofbirth.'">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Tanggal Lahir </label>
                                <input type="text" name="birthday" id="birthday" class="form-control" placeholder="Masukan Tanggal Lahir" value="'.$data->birthday.'">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Jenis Kelamin </label>
                                <select name="gender" id="gender" class="form-control" required="">
                                    '.$jk.'
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Alamat </label>
                                <input type="text" name="addres" id="addres" class="form-control" placeholder="Masukan Alamat" value="'.$data->addres.'">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label> Foto </label>
                                <input type="file" name="image" id="image" class="form-control" >
                            </div>
                        </div>
                    </div>

                    <hr>
                    <button type="submit" class="btn btn-info btn-fill pull-right">Simpan</button>
                    <div class="clearfix"></div>
                </form>
            ';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id             = $request->id;
        $nis            = $request->nis;
        $name           = $request->name;
        $placeofbirth   = $request->placeofbirth;
        $birthday       = $request->birthday;
        $gender         = $request->gender;
        if ($request->has('image')) {
            $member = explode(".",$request->file('image')->getClientOriginalName());
            $member = $nis."-".$data['name']."_".date("YmdHis").strtolower('.'.$member[1]);
            $tempat ="image/anggota";
            $request->file('image')->move($tempat,$member);
            $name_img=$tempat."/".$member;

            $data = member::where('id',$id)->update(array(
                'nis'            => $nis,
                'name'           => $name,
                'placeofbirth'   => $placeofbirth,
                'birthday'       => $birthday,
                'gender'         => $gender,
                'image'          => $name_img
            ));
        }else{
            $data = member::where('id',$id)->update(array(
                'nis'            => $nis,
                'name'           => $name,
                'placeofbirth'   => $placeofbirth,
                'birthday'       => $birthday,
                'gender'         => $gender
            ));
        }
        if ($data) {
            return redirect('member')->with('success','Data Anggota Berhasil di Ubah');
        }else{
            return redirect('member')->with('error','Data Anggota Gagal di Ubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data =member::find($id)->delete();
        if ($data) {
            return redirect('member')->with('success','Data Anggota Berhasil di Hapus');
        }else{
            return redirect('member')->with('error','Data Anggota Gagal di Hapus');
        }
    }
}
